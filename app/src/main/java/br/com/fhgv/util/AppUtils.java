package br.com.fhgv.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.Base64;

import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.fhgv.helpdeskapp.App;
import br.com.fhgv.helpdeskapp.CONST;
import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.helpdeskapp.domainws.User;


@SuppressWarnings({"unused"})
public class AppUtils {

    public static User getUser() {
        User user = null;
        SharedPreferences sharedPref = App.getContext().getSharedPreferences(getMessage(R.string.user), Context.MODE_PRIVATE);
        String json = sharedPref.getString("userData", null);
        if (json != null)
            user = new Gson().fromJson(json, User.class);
        return user;
    }

    public static boolean userIsTecnico() {
        User user = getUser();
        return user != null && user.isTecnico();
    }

    public static void saveUserInSharedPreferences(User user) {
        SharedPreferences sharedPref = App.getContext().getSharedPreferences(getMessage(R.string.user), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("userData", new Gson().toJson(user));
        editor.apply();
    }

    public static void removeUser() {
        SharedPreferences sharedPref = App.getContext().getSharedPreferences(getMessage(R.string.user), Context.MODE_PRIVATE);
        sharedPref.edit().clear().apply();
    }

    public static String brDate(Date date) {
        try {
            return new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).format(date);
        } catch (Exception e) {
            return null;
        }
    }

    public static String getMessage(int id) {
        return App.getContext().getResources().getString(id);
    }

    public static String getMessage(int id, Throwable throwable) {
        return String.format("%s %s", App.getContext().getResources().getString(id), throwable.getLocalizedMessage());
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static File getSdFolder(String folder) {
        File sd = new File(Environment.getExternalStorageDirectory(), folder);
        if (!sd.exists()) sd.mkdirs();
        return sd;
    }

    public static Uri getFileURI(Context context, File file) {
        return FileProvider.getUriForFile(context, CONST.FILE_PROVIDER_AUTHORITY, file);
    }

    @SuppressWarnings("WeakerAccess")
    public static File getFileFromImagesFolder(String relativePathOfFile) {
        return new File(getSdFolder(CONST.APP_IMAGES_FOLDER), relativePathOfFile);
    }

    public static byte[] getBytesFromFile(File file) {
        byte[] byteArray = null;
        try {
            InputStream inputStream = new FileInputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] b = new byte[1024 * 8];
            int bytesRead = 0;
            while ((bytesRead = inputStream.read(b)) != -1) {
                bos.write(b, 0, bytesRead);
            }
            byteArray = bos.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArray;
    }

    public static File saveBase64ToFile(String fileName, String dataInBase64) {
        File file = getFileFromImagesFolder(fileName);
        try (FileOutputStream out = new FileOutputStream(file)) {
            out.write(Base64.decode(dataInBase64, Base64.DEFAULT));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    public static Bitmap getBitmapFromImagesFolder(String relativePathOfFile) {
        return BitmapFactory.decodeFile(getFileFromImagesFolder(relativePathOfFile).getAbsolutePath());
    }

    @SuppressWarnings({"RegExpRedundantEscape", "ConstantExpression"})
    public static String getUrlFromString(String string) {
        Pattern urlPattern = Pattern.compile(
                "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
                Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
        Matcher matcher = urlPattern.matcher(string);
        String url = null;
        while (matcher.find()) {
            int matchStart = matcher.start(1);
            int matchEnd = matcher.end();
            url = string.substring(matchStart, matchEnd);
        }
        return url;
    }

    public static boolean isConnectedWifi(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork;
        if (cm != null) {
            activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) { // connected to the internet
                return (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI);
            }
        }
        return false;
    }
}