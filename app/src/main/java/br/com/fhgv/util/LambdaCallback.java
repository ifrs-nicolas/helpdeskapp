package br.com.fhgv.util;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;

import br.com.fhgv.helpdeskapp.domainws.JsonError;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LambdaCallback<T> implements Callback<T> {

    private SuccessCallback<T> success;
    private FailureCallback failure;

    public LambdaCallback(SuccessCallback<T> success, FailureCallback failure) {
        this.success = success;
        this.failure = failure;
    }

    public interface SuccessCallback<T> {
        void onResponse(T data);
    }

    public interface FailureCallback {
        void onFailure(Throwable t);
    }

    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            success.onResponse(response.body());
        } else {
            Throwable throwable;
            if (response.code() == 401) {
                throwable = new Throwable("requisição ao servidor não autorizada");
            } else {
                Log.d("SLA ERROR", response.toString());
                String msg = response.code() + " - " + response.message();
                try {
                    msg = new Gson().fromJson(response.errorBody().string(), JsonError.class).getError();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                throwable = new Throwable(msg);
            }
            failure.onFailure(throwable);
        }

    }
    public void onFailure(Call<T> call, Throwable t) {
        failure.onFailure(t);
    }
}
