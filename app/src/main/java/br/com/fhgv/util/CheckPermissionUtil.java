package br.com.fhgv.util;

import android.Manifest;
import android.app.Activity;
import android.util.Log;

public class CheckPermissionUtil {

    private static final int LOCATION_PERMISSION_REQ_CODE = 200;
    private static final int WRITE_SD_REQ_CODE = 201;
    private static final int READ_SD_REQ_CODE = 202;

    public static void checkLocation(Activity activity,
                                     PermissionUtil.ReqPermissionCallback callback) {
        PermissionUtil.checkPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION,
                LOCATION_PERMISSION_REQ_CODE,
                "We need location permission to locate your position",
                "We can't get your location without location permission",
                callback);
    }

    public static void checkWriteSd(Activity activity,
                                    PermissionUtil.ReqPermissionCallback callback) {
        PermissionUtil.checkPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                WRITE_SD_REQ_CODE,
                "We need write external storage permission to save your location to file",
                "We can't save your location to file without storage permission",
                callback);
    }

    public static void checkReadSd(Activity activity,
                                    PermissionUtil.ReqPermissionCallback callback) {
        PermissionUtil.checkPermission(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                READ_SD_REQ_CODE,
                "We need read external storage permission to read your location",
                "We can't save your location to file without storage permission",
                callback);
    }

    public static void checkCamera(final Activity activity,
                                   final PermissionUtil.ReqPermissionCallback callback) {
        checkLocation(activity, new PermissionUtil.ReqPermissionCallback() {
            public void onResult(boolean success) {
                Log.d("PERMISSAO LOCATION", success ? "CONCEDIDAS COM SUCESSO" : "NAO CONCEDIDAS");
                checkReadSd(activity, new PermissionUtil.ReqPermissionCallback() {
                    public void onResult(boolean success) {
                        Log.d("PERMISSAO READ SD", success ? "CONCEDIDAS COM SUCESSO" : "NAO CONCEDIDAS");
                        checkWriteSd(activity, new PermissionUtil.ReqPermissionCallback() {
                            public void onResult(boolean success) {
                                Log.d("PERMISSAO WRITE SD", success ? "CONCEDIDAS COM SUCESSO" : "NAO CONCEDIDAS");
                                callback.onResult(success);
                            }
                        });
                    }
                });
            }
        });
    }
}