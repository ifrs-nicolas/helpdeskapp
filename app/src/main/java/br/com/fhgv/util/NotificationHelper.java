package br.com.fhgv.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import java.util.List;

import br.com.fhgv.helpdeskapp.App;
import br.com.fhgv.helpdeskapp.R;

public class NotificationHelper {
    private Context mContext;
    private static final String NOTIFICATION_CHANNEL_ID = "10004";
    private Class<?> resultActivity;
    private Intent intent;

    public NotificationHelper(Context context, Class<?> cls) {
        mContext = context;
        resultActivity = cls;
    }

    public NotificationHelper(Context context, Intent intent) {
        this.mContext = context;
        this.intent = intent;
    }

    /**
     * Create and push the notification
     */
    @SuppressWarnings("deprecation")
    public void createNotification(String title, String message) {
        Intent resultIntent = null;

        /* Creates an explicit intent for an Activity in your app **/
        if (this.resultActivity != null) {
            resultIntent = new Intent(mContext, resultActivity);
            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        } else if (this.intent != null) {
            resultIntent = this.intent;
        }

        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                    0 /* Request code */, resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

        Uri soundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + App.getContext().getPackageName() + "/" + R.raw.msg);
//      RingtoneManager.getRingtone(App.getContext(), soundUri).play();
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                //.setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setSound(soundUri)
                .setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 250, 300, 400, 400, 300, 250, 400});
            notificationChannel.setSound(soundUri, new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build());
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);

            List<NotificationChannel> channelList = mNotificationManager.getNotificationChannels();
            for (int i = 0; channelList != null && i < channelList.size(); i++) {
                mNotificationManager.deleteNotificationChannel(channelList.get(i).getId());
            }
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(0 /* Request Code */, mBuilder.build());
    }
}