package br.com.fhgv.util;

import android.arch.lifecycle.MutableLiveData;

public class AsyncLiveData<T> extends MutableLiveData<T> {

    public AsyncLiveData() {}
    public AsyncLiveData(T value) {
        this.setValue(value);
    }
}
