package br.com.fhgv.helpdeskapp.domainws;

public class Status {

    private Integer id;
    private String nomeStatus;

    @Override
    public String toString() {
        return "Status{" +
                "id=" + id +
                ", nomeStatus='" + nomeStatus + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeStatus() {
        return nomeStatus;
    }

    public void setNomeStatus(String nomeStatus) {
        this.nomeStatus = nomeStatus;
    }
}
