package br.com.fhgv.helpdeskapp.domainws;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class IssueFilter implements Serializable {

    private int page;
    private int size;
    private String sortColumn;
    private String sortDir = "ASC";

    private Integer id;
    private String nome;
    private LocalDate dataAberturaChamadoInicial;
    private LocalDate dataAberturaChamadoFinal;
    private Integer usuarioSolicitanteId;
    private Integer tecnicoId;
    private Integer unidadeId;
    private Integer setorId;
    private boolean aguardandoAtendimento;
    private boolean emAtendimento;
    private boolean concluido;
    private boolean canceladoTi;
    private boolean canceladoUsuario;
    private boolean pendente;
    private boolean aguardandoAvaliacao;
    private List<Integer> atribuidoParaIds;
    private List<Integer> unidadesId;
    private List<Integer> tiposAtendimentosId;
    private String descricaoChamado;

    public IssueFilter() {
    }

    public IssueFilter apenasAbertos() {
        this.aguardandoAtendimento = true;
        this.emAtendimento = true;
        this.pendente = true;
        this.aguardandoAvaliacao = true;
        this.concluido = false;
        this.canceladoTi = false;
        this.canceladoUsuario = false;
        this.sortColumn = "id";
        this.sortDir = "ASC";
        this.size = 10;
        return this;
    }

    public IssueFilter apenasAbertosPorMim() {
        this.sortColumn = "id";
        this.sortDir = "DESC";
        this.size = 10;
        return this;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSortColumn() {
        return sortColumn;
    }

    public void setSortColumn(String sortColumn) {
        this.sortColumn = sortColumn;
    }

    public String getSortDir() {
        return sortDir;
    }

    public void setSortDir(String sortDir) {
        this.sortDir = sortDir;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataAberturaChamadoInicial() {
        return dataAberturaChamadoInicial;
    }

    public void setDataAberturaChamadoInicial(LocalDate dataAberturaChamadoInicial) {
        this.dataAberturaChamadoInicial = dataAberturaChamadoInicial;
    }

    public LocalDate getDataAberturaChamadoFinal() {
        return dataAberturaChamadoFinal;
    }

    public void setDataAberturaChamadoFinal(LocalDate dataAberturaChamadoFinal) {
        this.dataAberturaChamadoFinal = dataAberturaChamadoFinal;
    }

    public Integer getUsuarioSolicitanteId() {
        return usuarioSolicitanteId;
    }

    public void setUsuarioSolicitanteId(Integer usuarioSolicitanteId) {
        this.usuarioSolicitanteId = usuarioSolicitanteId;
    }

    public Integer getTecnicoId() {
        return tecnicoId;
    }

    public void setTecnicoId(Integer tecnicoId) {
        this.tecnicoId = tecnicoId;
    }

    public Integer getUnidadeId() {
        return unidadeId;
    }

    public void setUnidadeId(Integer unidadeId) {
        this.unidadeId = unidadeId;
    }

    public Integer getSetorId() {
        return setorId;
    }

    public void setSetorId(Integer setorId) {
        this.setorId = setorId;
    }

    public boolean isAguardandoAtendimento() {
        return aguardandoAtendimento;
    }

    public void setAguardandoAtendimento(boolean aguardandoAtendimento) {
        this.aguardandoAtendimento = aguardandoAtendimento;
    }

    public boolean isEmAtendimento() {
        return emAtendimento;
    }

    public void setEmAtendimento(boolean emAtendimento) {
        this.emAtendimento = emAtendimento;
    }

    public boolean isConcluido() {
        return concluido;
    }

    public void setConcluido(boolean concluido) {
        this.concluido = concluido;
    }

    public boolean isCanceladoTi() {
        return canceladoTi;
    }

    public void setCanceladoTi(boolean canceladoTi) {
        this.canceladoTi = canceladoTi;
    }

    public boolean isCanceladoUsuario() {
        return canceladoUsuario;
    }

    public void setCanceladoUsuario(boolean canceladoUsuario) {
        this.canceladoUsuario = canceladoUsuario;
    }

    public boolean isPendente() {
        return pendente;
    }

    public void setPendente(boolean pendente) {
        this.pendente = pendente;
    }

    public boolean isAguardandoAvaliacao() {
        return aguardandoAvaliacao;
    }

    public void setAguardandoAvaliacao(boolean aguardandoAvaliacao) {
        this.aguardandoAvaliacao = aguardandoAvaliacao;
    }

    public List<Integer> getAtribuidoParaIds() {
        return atribuidoParaIds;
    }

    public void setAtribuidoParaIds(List<Integer> atribuidoParaIds) {
        this.atribuidoParaIds = atribuidoParaIds;
    }

    public List<Integer> getUnidadesId() {
        return unidadesId;
    }

    public void setUnidadesId(List<Integer> unidadesId) {
        this.unidadesId = unidadesId;
    }

    public List<Integer> getTiposAtendimentosId() {
        return tiposAtendimentosId;
    }

    public void setTiposAtendimentosId(List<Integer> tiposAtendimentosId) {
        this.tiposAtendimentosId = tiposAtendimentosId;
    }

    public String getDescricaoChamado() {
        return descricaoChamado;
    }

    public void setDescricaoChamado(String descricaoChamado) {
        this.descricaoChamado = descricaoChamado;
    }
}
