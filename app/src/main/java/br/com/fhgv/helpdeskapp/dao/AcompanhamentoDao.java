package br.com.fhgv.helpdeskapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import br.com.fhgv.helpdeskapp.domainws.Acompanhamento;

@Dao
public interface AcompanhamentoDao {

    @Query("SELECT * FROM Acompanhamento WHERE atendimentoId = :atendimentoId")
    List<Acompanhamento> getAllByAtendimento(int atendimentoId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAll(List<Acompanhamento> issues);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(Acompanhamento issue);
}
