package br.com.fhgv.helpdeskapp.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import java.util.Objects;

import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.helpdeskapp.domainws.IssueFilter;
import br.com.fhgv.helpdeskapp.ui.abstracts.PrivateActivity;
import br.com.fhgv.helpdeskapp.ui.adapter.AtendimentoRecyclerAdapter;
import br.com.fhgv.helpdeskapp.ui.adapter.EndlessRecyclerViewScrollListener;
import br.com.fhgv.helpdeskapp.webservice.SLA;
import br.com.fhgv.util.LambdaCallback;

public class IssuesPanelActivity extends PrivateActivity {

    protected RecyclerView rvList;
    IssueFilter filter = new IssueFilter().apenasAbertos();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issues_panel);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.verifyUser()) {
            this.init();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        this.menu.findItem(R.id.issuePanel).setVisible(false);
        return true;
    }

    public void init() {
        this.rvList = findViewById(R.id.rvList);
        this.mountAdapter();
    }

    private void mountAdapter() {
        AtendimentoRecyclerAdapter adapter = new AtendimentoRecyclerAdapter();
        adapter.setLongItemClickListener(item -> {
            startActivity(new Intent(getApplicationContext(), IssueDetailsActivity.class)
                    .putExtra("atendimentoId", item.getId()));
            return true;
        });
        rvList.setAdapter(adapter);
        rvList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvList.setLayoutManager(linearLayoutManager);
        rvList.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        });
        this.loadNextDataFromApi(0);
    }

    public void loadNextDataFromApi(int offset) {
        Log.d("SLA ISSUES PANEL PAGE", offset + "");
        filter.setPage(offset);
        SLA.getIssuesWS().getAll(filter).enqueue(new LambdaCallback<>(data -> {
            if (data != null && rvList.getAdapter() != null) {
                ((AtendimentoRecyclerAdapter) Objects.requireNonNull(rvList.getAdapter())).addItens(data.getRecordsList());
                if (offset == 0) {
                    String msg = "Carregando " + data.getTotalElements() + " resultados.";
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            }
        }, t -> SLA.handleError(getApplicationContext(), t)));
    }
}

