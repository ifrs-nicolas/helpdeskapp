package br.com.fhgv.helpdeskapp.webservice;

import br.com.fhgv.helpdeskapp.domainws.Acompanhamento;
import br.com.fhgv.helpdeskapp.domainws.Atendimento;
import br.com.fhgv.helpdeskapp.domainws.AtendimentoCombos;
import br.com.fhgv.helpdeskapp.domainws.IssueFilter;
import br.com.fhgv.helpdeskapp.domainws.PageResponse;
import br.com.fhgv.helpdeskapp.domainws.RecordResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MyIssuesWS {

    @POST("api/sla/novo-chamado/read/")
    Call<PageResponse<Atendimento>> getAll(@Body IssueFilter filter);

    @GET("api/sla/novo-chamado/read/combos")
    Call<AtendimentoCombos> getCombos();

    @POST("api/sla/novo-chamado/create/")
    Call<RecordResponse<Atendimento>> insertNewIssue(@Body Atendimento atendimento);

      @PUT("api/sla/novo-chamado/update/cancelar/{id}")
    Call<RecordResponse<Atendimento>> cancelNewIssue(@Path("id") int id);

    @PUT("api/sla/novo-chamado/update/acompanhamento/save")
    Call<RecordResponse<Acompanhamento>> saveFollowUp(@Body Acompanhamento acompanhamento);

    @PUT("api/sla/novo-chamado/update/acompanhamento/delete")
    Call<RecordResponse<Acompanhamento>> deleteFollowUp(@Body Acompanhamento acompanhamento);
}
