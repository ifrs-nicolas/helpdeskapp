package br.com.fhgv.helpdeskapp.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.Gson;

import java.util.Timer;
import java.util.TimerTask;

import br.com.fhgv.helpdeskapp.App;
import br.com.fhgv.helpdeskapp.CONST;
import br.com.fhgv.helpdeskapp.domainws.RecordResponse;
import br.com.fhgv.helpdeskapp.ui.IssueDetailsActivity;
import br.com.fhgv.util.AppUtils;
import br.com.fhgv.util.NotificationHelper;
import ua.naiksoftware.stomp.Stomp;
import ua.naiksoftware.stomp.client.StompClient;

public class NovoChamadoService extends Service {

    private static final String TAG = "SLA STOMP SOCKET";
    private static Timer timer = new Timer();
    public static StompClient client;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.v(TAG, "SERVICE - onCreate()");
        super.onCreate();
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        Log.v(TAG, "SERVICE - onStartCommand()");
        if (AppUtils.userIsTecnico()){
            _startService();
        }
        return (super.onStartCommand(intent, flags, startId));//Continua ciclo de vida do meu serviço
    }

    private void _startService() {
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Log.d(TAG, "Socket reconnect....");
                if (client != null && client.isConnected()) client.disconnect();
                startWebSockets();
            }
        }, 1000, 30 * 60 * 1000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (client != null) client.disconnect();
    }

    @SuppressLint("CheckResult")
    private void startWebSockets() {
        connect();
        client.topic(CONST.WEBSOCKET_PAINEL).subscribe(message -> {
            Log.i(TAG, "Received message: " + message.getPayload());

            RecordResponse response = new Gson().fromJson(message.getPayload(), RecordResponse.class);
            if (response.getRecord() != null) {
                try {
                    Integer id = Double.valueOf(response.getRecord().toString()).intValue();

                    Intent intent = new Intent(getApplicationContext(), IssueDetailsActivity.class)
                            .putExtra("atendimentoId", id).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    new NotificationHelper(getApplicationContext(), intent)
                            .createNotification("FHGV - SLA", response.getMessage().getMessage());
                } catch (NumberFormatException e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        });
    }

    @SuppressLint("CheckResult")
    public static void connect() {
        client = Stomp.over(Stomp.ConnectionProvider.OKHTTP, CONST.SOCKET_URL);
        client.lifecycle().subscribe(lifecycleEvent -> {
            switch (lifecycleEvent.getType()) {
                case OPENED:
                    Log.d(TAG, "Stomp connection opened");
                    break;
                case CLOSED:
                    Log.d(TAG, "Stomp connection closed");
                    break;
                case ERROR:
                    Log.e(TAG, "Stomp connection error " + lifecycleEvent.getException().getMessage(), lifecycleEvent.getException());
                    break;
            }
        });
        client.connect();
    }

    @SuppressLint("CheckResult")
    public static void newIssueSendNotification(String atendimentoId) {
        if (client == null) {
            connect();
        }
        client.send(CONST.WEBSOCKET_NOVO_CHAMADO_CADASTRADO, atendimentoId).subscribe(
                () -> Log.d(TAG, "Sent data!"),
                error -> Log.e(TAG, "Encountered error while sending data!", error)
        );
    }
}
