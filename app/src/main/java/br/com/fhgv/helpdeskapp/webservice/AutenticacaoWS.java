package br.com.fhgv.helpdeskapp.webservice;

import br.com.fhgv.helpdeskapp.domainws.User;
import retrofit2.Call;
import retrofit2.http.GET;

public interface AutenticacaoWS {

    @GET("api/conf/autenticacao/read/usuario-autenticado")
    Call<User> getUser();
}
