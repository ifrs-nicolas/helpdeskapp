package br.com.fhgv.helpdeskapp.domainws;

import java.io.Serializable;

public class Setor implements Serializable {

    private Integer id;
    private String nomeSetor;
    private Unidade unidade;

    public Setor() {
    }

    public Setor(Integer id, String nomeSetor) {
        this.id = id;
        this.nomeSetor = nomeSetor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Setor setor = (Setor) o;
        return id != null ? id.equals(setor.id) : setor.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return getDesc();
    }

    public String getDesc() {
        if (this.unidade != null && this.getUnidade().getNomeUnidade() != null)
            return this.getUnidade().getSigla() + " - " + this.nomeSetor;
        else
            return this.getNomeSetor();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeSetor() {
        return nomeSetor;
    }

    public void setNomeSetor(String nomeSetor) {
        this.nomeSetor = nomeSetor;
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }
}
