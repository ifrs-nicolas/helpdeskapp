package br.com.fhgv.helpdeskapp.domainws;

import java.io.Serializable;

public class AtendimentoAnexo implements Serializable {

    private String nome;
    private String extensao;
    private String tipo;
    private String arquivo;

    public String getFileName() {
        return (this.nome + "."
                + (this.extensao == null ? "tmp" : this.extensao.toLowerCase()))
                .replaceAll(" ", "_");
    }

    @Override
    public String toString() {
        return "AtendimentoAnexo{" +
                "nome='" + nome + '\'' +
                ", extensao='" + extensao + '\'' +
                ", tipo='" + tipo + '\'' +
                ", arquivo='" + arquivo + '\'' +
                '}';
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getExtensao() {
        return extensao;
    }

    public void setExtensao(String extensao) {
        this.extensao = extensao;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }

}
