package br.com.fhgv.helpdeskapp.domainws;

import java.io.Serializable;

public class TipoAtendimento implements Serializable {

    private Integer id;
    private String tipo;
    private String descricao;

    public TipoAtendimento() {
    }

    public TipoAtendimento(Integer id, String tipo) {
        this.id = id;
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return tipo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
