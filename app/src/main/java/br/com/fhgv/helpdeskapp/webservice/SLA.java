package br.com.fhgv.helpdeskapp.webservice;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.fhgv.helpdeskapp.CONST;
import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.helpdeskapp.domainws.User;
import br.com.fhgv.util.AppUtils;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SLA {

    static OkHttpClient client;
    static Gson gson;

    public static void login(String username, String password) {
        client = new OkHttpClient.Builder().addInterceptor(new BasicAuthInterceptor(username, password)).build();
    }

    private static Retrofit getRetrofit() {
        if (client == null) {
            User user = AppUtils.getUser();
            if (user != null) {
                login(user.getLogin(), user.getSenha());
            }
        }
        if (gson == null) {
            gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
        }
        return new Retrofit.Builder().baseUrl(CONST.WEBSERVICE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }
    
    public static void handleError(Context context, Throwable t) {
        Log.e("SLA ERROR", t.getMessage());
        Toast.makeText(context, String.format("%s: %s", context.getString(R.string.error), t.getLocalizedMessage()), Toast.LENGTH_SHORT).show();
    }

    public static AutenticacaoWS getAutenticacaoWS() {
        return getRetrofit().create(AutenticacaoWS.class);
    }

    public static MyIssuesWS getMyIssuesWS() {
        return getRetrofit().create(MyIssuesWS.class);
    }

    public static IssuesWS getIssuesWS() {
        return getRetrofit().create(IssuesWS.class);
    }
}
