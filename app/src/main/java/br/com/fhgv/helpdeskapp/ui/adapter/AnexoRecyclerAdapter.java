package br.com.fhgv.helpdeskapp.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.helpdeskapp.domainws.AtendimentoAnexo;

public class AnexoRecyclerAdapter extends RecyclerView.Adapter<AnexoRecyclerAdapter.AtendimentoAnexoLineHolder> {

    private Context context;
    private List<AtendimentoAnexo> list;
    private OnItemClickListener onItemClickListener;

    public AnexoRecyclerAdapter(Context context, List<AtendimentoAnexo> list) {
        this.context = context;
        this.list = list;
    }

    public void addItem(AtendimentoAnexo item) {
        if (list == null) {
            list = new ArrayList<>();
        }
        int oldSize = getItemCount();
        this.list.add(item);
        this.notifyItemRangeInserted(oldSize, getItemCount());
    }

    public interface OnItemClickListener {
        void onItemClick(AtendimentoAnexo item);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    @NonNull
    public AtendimentoAnexoLineHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AtendimentoAnexoLineHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.anexo_line_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AtendimentoAnexoLineHolder viewHolder, int position) {
        if (list != null) viewHolder.loadData(list.get(position));
    }

    public class AtendimentoAnexoLineHolder extends RecyclerView.ViewHolder {

        TextView tvInfo;
        ImageView ivAnexo;

        private AtendimentoAnexoLineHolder(View itemView) {
            super(itemView);
            tvInfo = itemView.findViewById(R.id.tvInfo);
            ivAnexo = itemView.findViewById(R.id.ivAnexo);
        }

        private void loadData(AtendimentoAnexo item) {
            this.tvInfo.setText(item.getFileName());
            this.itemView.setOnClickListener(v -> onItemClickListener.onItemClick(item));
        }
    }
}
