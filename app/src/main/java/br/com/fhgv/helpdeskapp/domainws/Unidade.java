package br.com.fhgv.helpdeskapp.domainws;

import java.io.Serializable;

public class Unidade implements Serializable {

    private Integer id;
    private String nomeUnidade;
    private String sigla;
    private String fone;

    public Unidade() {
    }

    public Unidade(Integer id, String nomeUnidade) {
        this.id = id;
        this.nomeUnidade = nomeUnidade;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unidade unidade = (Unidade) o;
        return id != null ? id.equals(unidade.id) : unidade.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    @Override
    public String toString() {
        return nomeUnidade;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeUnidade() {
        return nomeUnidade;
    }

    public void setNomeUnidade(String nomeUnidade) {
        this.nomeUnidade = nomeUnidade;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }
}
