package br.com.fhgv.helpdeskapp.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import br.com.fhgv.helpdeskapp.App;
import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.helpdeskapp.domainws.Acompanhamento;
import br.com.fhgv.helpdeskapp.domainws.Atendimento;
import br.com.fhgv.helpdeskapp.domainws.AtendimentoAnexo;
import br.com.fhgv.helpdeskapp.ui.abstracts.PrivateActivity;
import br.com.fhgv.helpdeskapp.ui.adapter.AcompanhamentoRecyclerAdapter;
import br.com.fhgv.helpdeskapp.ui.adapter.AnexoRecyclerAdapter;
import br.com.fhgv.helpdeskapp.webservice.SLA;
import br.com.fhgv.util.AppUtils;
import br.com.fhgv.util.CheckPermissionUtil;
import br.com.fhgv.util.LambdaCallback;
import br.com.fhgv.util.PermissionUtil;

public class IssueDetailsActivity extends PrivateActivity {

    public TextView tvId, tvSolicitante, tvStatus, tvSetor, tvDtAbertura, tvDesc, tvIp;
    public RecyclerView rvAcompanhamentos, rvAnexos;
    private Integer atendimentoId;
    private MenuItem btAtender, btConcluir;

    private String currentPhotoPath = null;
    private Atendimento atendimento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issue_details);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.verifyUser()) {
            this.init();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (AppUtils.userIsTecnico()) {
            getMenuInflater().inflate(R.menu.menu_issue_tecnico, menu);
        } else {
            getMenuInflater().inflate(R.menu.menu_private, menu);
        }
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btAtender:
                iniciarAtendimento();
                return true;
            case R.id.btConcluir:
                concluirAtendimento();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
    }

    public void init() {
        CheckPermissionUtil.checkCamera(this, success -> Log.d("PERMISSAO PARA CAMERA",
                success ? "CONCEDIDAS COM SUCESSO" : "NAO CONCEDIDAS"));
        this.atendimentoId = getIntent().getIntExtra("atendimentoId", 0);
        this.tvId = findViewById(R.id.tv1);
        this.tvSolicitante = findViewById(R.id.tvSolicitante);
        this.tvStatus = findViewById(R.id.tvInfo);
        this.tvSetor = findViewById(R.id.tvSetor);
        this.tvDesc = findViewById(R.id.tvTexto);
        this.tvDtAbertura = findViewById(R.id.tvDtAbertura);
        this.tvIp = findViewById(R.id.tvIp);
        this.rvAcompanhamentos = findViewById(R.id.rvAcompanhamentos);
        this.rvAnexos = findViewById(R.id.rvAnexos);
        if (atendimentoId != 0) {
            this.loadIssue();
        }

        FloatingActionButton sendAcompanhamento = findViewById(R.id.btSend);
        sendAcompanhamento.setOnClickListener(view -> {
            Acompanhamento acompanhamento = new Acompanhamento();
            acompanhamento.setAtendimentoId(atendimentoId);
            acompanhamento.setTecnico(user);
            EditText acomp = findViewById(R.id.edAcompanhamento);
            acompanhamento.setTexto(acomp.getText().toString());
            saveFollowUp(acompanhamento);
        });


    }

    @SuppressLint("DefaultLocale")
    public void fillFields(Atendimento atendimento) {
        if (atendimento != null) {
            this.atendimento = atendimento;
            this.tvSolicitante.setText(atendimento.getNome());
            this.tvStatus.setText(atendimento.getStatus() == null ? "" : atendimento.getStatus().getNomeStatus());
            this.tvSetor.setText(atendimento.getSetor() == null ? "" : atendimento.getSetor().getDesc());
            this.tvDtAbertura.setText(AppUtils.brDate(atendimento.getDataAberturaChamado()));
            this.tvDesc.setText(atendimento.getDescricaoChamado());
            this.tvId.setText(String.format("%d", atendimento.getId()));
            this.tvIp.setText(atendimento.getEnderecoIp());

            if (atendimento.getAcompanhamentos() != null) {
                this.rvAcompanhamentos.setAdapter(new AcompanhamentoRecyclerAdapter(atendimento.getAcompanhamentos()));
                this.rvAcompanhamentos.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
                this.rvAcompanhamentos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            }

            if (AppUtils.userIsTecnico() && menu != null) {
                this.btAtender = menu.findItem(R.id.btAtender);
                this.btConcluir = menu.findItem(R.id.btConcluir);
                this.btAtender.setVisible(false);
                this.btConcluir.setVisible(false);
                Integer statusId = atendimento.getStatus().getId();
                if (statusId == 1) {
                    this.btAtender.setVisible(true);
                }
                if (statusId == 2 || statusId == 6) {
                    this.btConcluir.setVisible(true);
                }

            }
            this.loadAnexosAdapter(atendimento);
        }

    }

    private void loadAnexosAdapter(Atendimento atendimento) {
        if (atendimento.getAnexos() == null) {
            atendimento.setAnexos(new ArrayList<>());
        }
        AnexoRecyclerAdapter adapter = new AnexoRecyclerAdapter(getApplicationContext(), atendimento.getAnexos());
        adapter.setOnItemClickListener(item -> {
            String tempName = ("temp." + (item.getExtensao() == null ? "tmp" : item.getExtensao().toLowerCase()));
            File tempFile = AppUtils.saveBase64ToFile(tempName, item.getArquivo());
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Log.d("SLA tipo", item.getTipo());
            intent.setDataAndType(AppUtils.getFileURI(App.getContext(), tempFile), item.getTipo());
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivity(intent);
        });
        this.rvAnexos.setAdapter(adapter);
        this.rvAnexos.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        this.rvAnexos.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    private void loadIssue() {
        SLA.getIssuesWS().getById(this.atendimentoId).enqueue(new LambdaCallback<>(data -> {
            if (data != null) {
                Log.d("SLA ISSUE ID ", atendimentoId.toString());
                fillFields(data.getRecord());
                if (data.getMessage() != null && data.getMessage().getMessage() != null)
                    Toast.makeText(getApplicationContext(), getString(R.string.success) + ": "
                            + data.getMessage().getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), AppUtils.getMessage(R.string.error) + ": nenhum resultado.", Toast.LENGTH_SHORT).show();
            }
        }, t -> SLA.handleError(getApplicationContext(), t)));
    }

    private void saveFollowUp(Acompanhamento acompanhamento) {
        if (acompanhamento.getTexto().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), AppUtils.getMessage(R.string.error)
                    + ": O conteúdo do acompanhamento deve ser infomado.", Toast.LENGTH_SHORT).show();
        } else {
            SLA.getIssuesWS().saveFollowUp(acompanhamento).enqueue(new LambdaCallback<>(data -> {
                if (data != null) {
                    Log.d("SLA ISSUE ID ", atendimentoId.toString());
                    if (data.getMessage() != null && data.getMessage().getMessage() != null) {
                        Toast.makeText(getApplicationContext(), data.getMessage().toString(), Toast.LENGTH_SHORT).show();
                    }
                    EditText edAcompanhamento = findViewById(R.id.edAcompanhamento);
                    edAcompanhamento.getText().clear();
                    ((AcompanhamentoRecyclerAdapter) Objects.requireNonNull(this.rvAcompanhamentos.getAdapter())).addItem(data.getRecord());
                } else {
                    Toast.makeText(getApplicationContext(), AppUtils.getMessage(R.string.error) + ": nenhum resultado.", Toast.LENGTH_SHORT).show();
                }
            }, t -> SLA.handleError(getApplicationContext(), t)));
        }
    }

    @SuppressLint("SimpleDateFormat")
    public void takePicture(View view) {
        CheckPermissionUtil.checkCamera(this, success -> {
            Log.d("PERMISSAO PARA CAMERA",
                    success ? "CONCEDIDAS COM SUCESSO" : "NAO CONCEDIDAS");

            currentPhotoPath = String.format("IMG_%s.jpg", new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()));
            Uri uri = AppUtils.getFileURI(getApplicationContext(), AppUtils.getFileFromImagesFolder(currentPhotoPath));
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, CAMERA_REQUEST);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intentReturn) {
        if (intentReturn != null && requestCode == CAMERA_REQUEST) {
            File file = AppUtils.getFileFromImagesFolder(currentPhotoPath);
            AtendimentoAnexo anexo = new AtendimentoAnexo();
            anexo.setNome(file.getName().replace(".jpg", ""));
            anexo.setTipo("image/jpg");
            anexo.setExtensao("jpg");
            anexo.setArquivo(Base64.encodeToString(AppUtils.getBytesFromFile(file), Base64.DEFAULT));
            this.saveAnexo(anexo);
        } else {
            PermissionUtil.onActivityResult(this, requestCode);
        }
    }

    private void saveAnexo(AtendimentoAnexo anexo) {
        Log.d("SLA SENDING", "enviando a foto");
        SLA.getIssuesWS().uploadAnexo(this.atendimentoId, anexo).enqueue(new LambdaCallback<>(data -> {
            if (data != null) {
                Log.d("SLA ANEXO NAME", anexo.getFileName());
                if (data.getMessage() != null && data.getMessage().getMessage() != null) {
                    Toast.makeText(getApplicationContext(), getString(R.string.success) + ": "
                            + data.getMessage().getMessage(), Toast.LENGTH_SHORT).show();
                }
//              if (atendimento.getAnexos() == null)
//                atendimento.setAnexos(new ArrayList<>());
//              atendimento.getAnexos().add(data.getRecord());
                ((AnexoRecyclerAdapter) Objects.requireNonNull(this.rvAnexos.getAdapter())).addItem(data.getRecord());
            }
        }, t -> SLA.handleError(getApplicationContext(), t)));
    }

    private void iniciarAtendimento() {
        SLA.getIssuesWS().iniciarAtendimento(this.atendimentoId).enqueue(new LambdaCallback<>(data -> {
            if (data != null) {
                Log.d("SLA ISSUE ID ", atendimentoId.toString());
                fillFields(data.getRecord());
                if (data.getMessage() != null && data.getMessage().getMessage() != null)
                    Toast.makeText(getApplicationContext(), getString(R.string.success) + ": "
                            + data.getMessage().getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), AppUtils.getMessage(R.string.error) + ": nenhum resultado.", Toast.LENGTH_SHORT).show();
            }
        }, t -> SLA.handleError(getApplicationContext(), t)));
    }

    private void concluirAtendimento() {
        Atendimento a = new Atendimento();
        a.setEnderecoIp(this.atendimento.getEnderecoIp());
        SLA.getIssuesWS().concluirAtendimento(this.atendimentoId, a).enqueue(new LambdaCallback<>(data -> {
            if (data != null) {
                Log.d("SLA ISSUE ID ", atendimentoId.toString());
                fillFields(data.getRecord());
                if (data.getMessage() != null && data.getMessage().getMessage() != null)
                    Toast.makeText(getApplicationContext(), getString(R.string.success) + ": "
                            + data.getMessage().getMessage(), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), AppUtils.getMessage(R.string.error) + ": nenhum resultado.", Toast.LENGTH_SHORT).show();
            }
        }, t -> SLA.handleError(getApplicationContext(), t)));
    }

}
