package br.com.fhgv.helpdeskapp.domainws;

import java.util.Date;

public class Acompanhamento {

    private Integer id;
    private String texto;
    private Integer atendimentoId;
    private Date dataCadastro;
    private Date dataAtualizacao;
    private User tecnico; // é o autor do acompanhamento

    @Override
    public String toString() {
        return "Acompanhamento{" +
                "id=" + id +
                ", texto='" + texto + '\'' +
                ", atendimentoId=" + atendimentoId +
                ", dataCadastro=" + dataCadastro +
                ", dataAtualizacao=" + dataAtualizacao +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Integer getAtendimentoId() {
        return atendimentoId;
    }

    public void setAtendimentoId(Integer atendimentoId) {
        this.atendimentoId = atendimentoId;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public User getTecnico() {
        return tecnico;
    }

    public void setTecnico(User tecnico) {
        this.tecnico = tecnico;
    }
}
