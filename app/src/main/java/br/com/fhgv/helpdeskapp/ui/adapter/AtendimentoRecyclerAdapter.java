package br.com.fhgv.helpdeskapp.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.helpdeskapp.domainws.Atendimento;
import br.com.fhgv.util.AppUtils;

public class AtendimentoRecyclerAdapter extends RecyclerView.Adapter<AtendimentoRecyclerAdapter.AtendimentoLineHolder> {

    private List<Atendimento> list;
    private OnItemClickListener clickListener;
    private OnLongClickListener longClickListener;

    public AtendimentoRecyclerAdapter() {
        this.list = new ArrayList<>();
    }

    public void addItens(List<Atendimento> list) {
        if (list != null && list.size() > 0) {
            int oldSize = getItemCount();
            this.list.addAll(list);
            this.notifyItemRangeInserted(oldSize, getItemCount());
        }
    }

    public void setClickListener(OnItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setLongItemClickListener(OnLongClickListener longClickListener) {
        this.longClickListener = longClickListener;
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    @NonNull
    public AtendimentoLineHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AtendimentoLineHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.issue_line_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AtendimentoLineHolder viewHolder, int position) {
        if (list != null) viewHolder.loadData(list.get(position));
    }

    public interface OnItemClickListener {
        void onItemClick(Atendimento item);
    }

    public interface OnLongClickListener {
        boolean onLongItemClick(Atendimento item);
    }

    public class AtendimentoLineHolder extends RecyclerView.ViewHolder {

        public TextView tvId, tvSolicitante, tvStatus, tvSetor, tvDtAbertura, tvDesc, tvIp;

        private AtendimentoLineHolder(View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.tv1);
            tvSolicitante = itemView.findViewById(R.id.tvSolicitante);
            tvStatus = itemView.findViewById(R.id.tvInfo);
            tvSetor = itemView.findViewById(R.id.tvSetor);
            tvDesc = itemView.findViewById(R.id.tvTexto);
            tvDtAbertura = itemView.findViewById(R.id.tvDtAbertura);
            tvIp = itemView.findViewById(R.id.tvIp);
        }

        private void loadData(Atendimento item) {
            this.tvId.setText("" + item.getId());
            this.tvSolicitante.setText(item.getNome());
            this.tvStatus.setText(item.getStatus() == null ? "" : item.getStatus().getNomeStatus());
            this.tvSetor.setText(item.getSetor() == null ?  "" : item.getSetor().getDesc());
            this.tvDtAbertura.setText(AppUtils.brDate(item.getDataAberturaChamado()));
            this.tvIp.setText(item.getEnderecoIp());

            final String compactDesc = item.getDescricaoChamado().length() > 100 ? item.getDescricaoChamado().substring(0, 100) + "..." : item.getDescricaoChamado();
            this.tvDesc.setText(compactDesc);

            this.itemView.setOnClickListener(v -> {
                tvDesc.setText(item.clicado ? compactDesc : item.getDescricaoChamado());
                item.clicado = !item.clicado;

                if (clickListener != null)
                    clickListener.onItemClick(item);
            });

            if (longClickListener != null)
                this.itemView.setOnLongClickListener(v -> longClickListener.onLongItemClick(item));
        }
    }
}
