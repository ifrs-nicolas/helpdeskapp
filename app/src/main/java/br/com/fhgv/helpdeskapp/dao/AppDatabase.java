package br.com.fhgv.helpdeskapp.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import br.com.fhgv.helpdeskapp.App;
import br.com.fhgv.helpdeskapp.domain.LocalIssue;

@Database(entities = {LocalIssue.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    private static final String DB_NAME = "database-helpdeskapp";
    private static volatile AppDatabase instance;

    public static synchronized AppDatabase getInstance() {
        return getInstance(App.getContext());
    }

    private static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = create(context);
        }
        return instance;
    }

    private static AppDatabase create(final Context context) {
        return Room.databaseBuilder(
                context,
                AppDatabase.class,
                DB_NAME)
                .allowMainThreadQueries()
//                .fallbackToDestructiveMigration()
//                .addMigrations(MIGRATION_1_2)
                .build();
    }

/*    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE note ADD COLUMN image VARCHAR");
        }
    };*/

    public abstract LocalIssueDao issueDao();
}

