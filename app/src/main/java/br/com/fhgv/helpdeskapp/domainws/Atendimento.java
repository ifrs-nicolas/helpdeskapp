package br.com.fhgv.helpdeskapp.domainws;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Atendimento implements Serializable {

    private Integer id;
    private short numVersion;
    private String nome; // É o nome do solicitante do atendimento
    private Unidade unidade;
    private Area area;
    private Setor setor;
    private Status status;
    private String fone;
    private String ramal;
    private Date dataAberturaChamado;
    private Date dataInicioChamado;
    private Date dataFimAtendimento;
    private User tecnico;
    private User usuarioSolicitante;
    private String descricaoChamado;
    private String enderecoIp;
    private List<Acompanhamento> acompanhamentos;
    private Acompanhamento acompanhamento;
    private TipoAtendimento tipoAtendimento;
    private List<AtendimentoAnexo> anexos;
    private List<AtendimentoAnexo> anexosTemp;

    public boolean clicado = false;

    public Atendimento() {
    }

    public Atendimento(String descricaoChamado) {
        this.descricaoChamado = descricaoChamado;
    }

    @Override
    public String toString() {
        return "Atendimento{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", descricaoChamado='" + descricaoChamado + '\'' +
                ", enderecoIp='" + enderecoIp + '\'' +
                ", dataAberturaChamado=" + dataAberturaChamado +
                ", setor=" + setor +
                ", status=" + status +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Unidade getUnidade() {
        return unidade;
    }

    public void setUnidade(Unidade unidade) {
        this.unidade = unidade;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Setor getSetor() {
        return setor;
    }

    public void setSetor(Setor setor) {
        this.setor = setor;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getFone() {
        return fone;
    }

    public void setFone(String fone) {
        this.fone = fone;
    }

    public String getRamal() {
        return ramal;
    }

    public void setRamal(String ramal) {
        this.ramal = ramal;
    }

    public Date getDataAberturaChamado() {
        return dataAberturaChamado;
    }

    public void setDataAberturaChamado(Date dataAberturaChamado) {
        this.dataAberturaChamado = dataAberturaChamado;
    }

    public Date getDataInicioChamado() {
        return dataInicioChamado;
    }

    public void setDataInicioChamado(Date dataInicioChamado) {
        this.dataInicioChamado = dataInicioChamado;
    }

    public Date getDataFimAtendimento() {
        return dataFimAtendimento;
    }

    public void setDataFimAtendimento(Date dataFimAtendimento) {
        this.dataFimAtendimento = dataFimAtendimento;
    }

    public User getTecnico() {
        return tecnico;
    }

    public void setTecnico(User tecnico) {
        this.tecnico = tecnico;
    }

    public User getUsuarioSolicitante() {
        return usuarioSolicitante;
    }

    public void setUsuarioSolicitante(User usuarioSolicitante) {
        this.usuarioSolicitante = usuarioSolicitante;
    }

    public String getDescricaoChamado() {
        return descricaoChamado;
    }

    public void setDescricaoChamado(String descricaoChamado) {
        this.descricaoChamado = descricaoChamado;
    }

    public String getEnderecoIp() {
        return enderecoIp;
    }

    public void setEnderecoIp(String enderecoIp) {
        this.enderecoIp = enderecoIp;
    }

    public List<Acompanhamento> getAcompanhamentos() {
        return acompanhamentos;
    }

    public void setAcompanhamentos(List<Acompanhamento> acompanhamentos) {
        this.acompanhamentos = acompanhamentos;
    }

    public Acompanhamento getAcompanhamento() {
        return acompanhamento;
    }

    public void setAcompanhamento(Acompanhamento acompanhamento) {
        this.acompanhamento = acompanhamento;
    }

    public TipoAtendimento getTipoAtendimento() {
        return tipoAtendimento;
    }

    public void setTipoAtendimento(TipoAtendimento tipoAtendimento) {
        this.tipoAtendimento = tipoAtendimento;
    }

    public List<AtendimentoAnexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<AtendimentoAnexo> anexos) {
        this.anexos = anexos;
    }

    public List<AtendimentoAnexo> getAnexosTemp() {
        return anexosTemp;
    }

    public void setAnexosTemp(List<AtendimentoAnexo> anexosTemp) {
        this.anexosTemp = anexosTemp;
    }

    public short getNumVersion() {
        return numVersion;
    }

    public void setNumVersion(short numVersion) {
        this.numVersion = numVersion;
    }
}
