package br.com.fhgv.helpdeskapp.ui.abstracts;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.util.AppUtils;
import br.com.fhgv.util.PermissionUtil;

public abstract class BaseActivity extends AppCompatActivity {

    protected Menu menu;
    protected final static int CAMERA_REQUEST = 1368;

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        PermissionUtil.onRequestPermissionResult(this, requestCode, permissions, grantResults);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_public, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_go_back:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
