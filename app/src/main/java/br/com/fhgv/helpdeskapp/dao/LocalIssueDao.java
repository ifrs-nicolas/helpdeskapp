package br.com.fhgv.helpdeskapp.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import br.com.fhgv.helpdeskapp.domain.LocalIssue;
import br.com.fhgv.helpdeskapp.domainws.Atendimento;

@Dao
public interface LocalIssueDao {

    @Query("SELECT * FROM LocalIssue WHERE usuarioSolicitanteId = :userId")
    List<LocalIssue> getAllByUser(int userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void saveAll(List<LocalIssue> issues);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(LocalIssue issue);
}
