package br.com.fhgv.helpdeskapp.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.helpdeskapp.domainws.Area;
import br.com.fhgv.helpdeskapp.domainws.Atendimento;
import br.com.fhgv.helpdeskapp.domainws.AtendimentoCombos;
import br.com.fhgv.helpdeskapp.domainws.Setor;
import br.com.fhgv.helpdeskapp.domainws.TipoAtendimento;
import br.com.fhgv.helpdeskapp.domainws.Unidade;
import br.com.fhgv.helpdeskapp.service.NovoChamadoService;
import br.com.fhgv.helpdeskapp.ui.abstracts.PrivateActivity;
import br.com.fhgv.helpdeskapp.webservice.SLA;
import br.com.fhgv.util.LambdaCallback;

public class NewIssueActivity extends PrivateActivity {

    protected EditText edDesc;
    protected EditText edIp;
    protected EditText edPExtension;
    protected EditText edPhone;
    protected EditText edRequester;
    protected Spinner spTipoAtendimento;
    protected Spinner spUnidade;
    protected Spinner spSetor;
    protected Spinner spArea;
    protected AtendimentoCombos combos;
    private boolean travar = false;

    private static final String TAG = "SLA NewIssueActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_issue);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.verifyUser()) {
            this.init();
            // teste
            // NovoChamadoService.newIssueSendNotification("9845");
        }
    }

    @SuppressLint("SetTextI18n")
    public void init() {
        edDesc = findViewById(R.id.edDesc);
        edIp = findViewById(R.id.edIp);
        edPExtension = findViewById(R.id.edPExtension);
        edPhone = findViewById(R.id.edPhone);
        edRequester = findViewById(R.id.edRequester);
        spTipoAtendimento = findViewById(R.id.spTipoAtendimento);
        spUnidade = findViewById(R.id.spUnidade);
        spSetor = findViewById(R.id.spSetor);
        spArea = findViewById(R.id.spArea);
        spUnidade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Unidade unidade = (Unidade) parent.getItemAtPosition(position);
                if (!travar) {
                    loadSetoresAndAreasPorUnidade(unidade, false);
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        this.initialData();
        this.loadCombos();
    }

    private void travarUnidadeListener() {
        this.travar = true;
    }

    private void destravarUnidadeListener() {
        this.travar = false;
    }

    public void resetSetorAndArea() {
        ArrayAdapter<Setor> setoresAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, new ArrayList<>());
        setoresAdapter.insert(new Setor(0, "Selecione uma unidade antes"), 0);
        spSetor.setAdapter(setoresAdapter);

        ArrayAdapter<Area> areasAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, new ArrayList<>());
        areasAdapter.insert(new Area(0, "Selecione uma unidade antes"), 0);
        spArea.setAdapter(areasAdapter);
    }

    public void loadSetoresAndAreasPorUnidade(Unidade unidade, boolean loadFromUser) {
        if (unidade != null && unidade.getId() != null) {
            if (unidade.getId() == 0) {
                resetSetorAndArea();
            } else {
                Log.d("SLA unidade selecionada", "" + unidade.getNomeUnidade());

                edPhone.setText(unidade.getFone());
                List<Setor> setores = new ArrayList<>();
                for (Setor setor : combos.getSetores()) {
                    if (unidade.getId().equals(setor.getUnidade().getId())) {
                        setores.add(setor);
                    }
                }
                ArrayAdapter<Setor> setoresAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, setores);
                setoresAdapter.insert(new Setor(0, "Selecione"), 0);
                spSetor.setAdapter(setoresAdapter);

                List<Area> areas = new ArrayList<>();
                for (Area area : combos.getAreas()) {
                    if (unidade.getId().equals(area.getUnidade().getId())) {
                        areas.add(area);
                    }
                }
                ArrayAdapter<Area> areasAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, areas);
                areasAdapter.insert(new Area(0, "Selecione"), 0);
                spArea.setAdapter(areasAdapter);

                if (loadFromUser) {
                    Log.d("SLA", "carregando setor e area do usuario");
                    spSetor.setSelection(setoresAdapter.getPosition(user.getSetor()));
                    spArea.setSelection(areasAdapter.getPosition(user.getArea()));
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    public void cleanForm(View view) {
        edDesc.getText().clear();
        edPExtension.getText().clear();
        edPhone.getText().clear();
        edRequester.getText().clear();
        spTipoAtendimento.setSelection(0);
        this.initialData();
        this.carregaUnidadeDoUsuario();
    }

    public void initialData() {
        edIp.setText("192.168.");
        edRequester.setText(user.getNome());
        edPExtension.setText(user.getRamal());
    }

    public void carregaUnidadeDoUsuario() {
        if (spUnidade.getAdapter() != null) {
            this.travarUnidadeListener();
            this.spUnidade.setSelection(((ArrayAdapter<Unidade>) spUnidade.getAdapter()).getPosition(this.user.getUnidade()));
            this.loadSetoresAndAreasPorUnidade(this.user.getUnidade(), true);
            (new Handler()).postDelayed(this::destravarUnidadeListener, 2000);
        }
    }

    public void loadCombos() {
        SLA.getMyIssuesWS().getCombos().enqueue(new LambdaCallback<>(data -> {
            if (data != null) {
                this.combos = data;

                ArrayAdapter<TipoAtendimento> tiposAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, data.getTiposAtendimentos());
                tiposAdapter.insert(new TipoAtendimento(0, "Selecione"), 0);
                spTipoAtendimento.setAdapter(tiposAdapter);

                ArrayAdapter<Unidade> unidadesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, data.getUnidades());
                unidadesAdapter.insert(new Unidade(0, "Selecione"), 0);
                spUnidade.setAdapter(unidadesAdapter);
                carregaUnidadeDoUsuario();
            }
        }, t -> SLA.handleError(getApplicationContext(), t)));
    }

    public void send(View view) {
        Atendimento atendimento = new Atendimento();
        atendimento.setDescricaoChamado(edDesc.getText().toString());
        atendimento.setEnderecoIp(edIp.getText().toString());
        atendimento.setRamal(edPExtension.getText().toString());
        atendimento.setFone(edPhone.getText().toString());
        atendimento.setNome(edRequester.getText().toString());
        atendimento.setTipoAtendimento((TipoAtendimento) spTipoAtendimento.getSelectedItem());
        atendimento.setUnidade((Unidade) spUnidade.getSelectedItem());
        atendimento.setSetor((Setor) spSetor.getSelectedItem());
        atendimento.setArea((Area) spArea.getSelectedItem());
        SLA.getMyIssuesWS().insertNewIssue(atendimento).enqueue(new LambdaCallback<>(data -> {
            if (data != null) {
                Toast.makeText(getApplicationContext(), data.getMessage().toString(), Toast.LENGTH_SHORT).show();

                Integer id = data.getRecord().getId();
                NovoChamadoService.newIssueSendNotification(id.toString());
                startActivity(new Intent(getApplicationContext(), IssueDetailsActivity.class).putExtra("atendimentoId", id));
            }
        }, t -> SLA.handleError(getApplicationContext(), t)));
    }
}
