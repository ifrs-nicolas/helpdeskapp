package br.com.fhgv.helpdeskapp.domainws;

public class RecordResponse<T> {

    private T record;
    private Message message;

    public T getRecord() {
        return record;
    }

    public void setRecord(T record) {
        this.record = record;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
