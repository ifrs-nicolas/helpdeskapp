package br.com.fhgv.helpdeskapp.webservice;

import br.com.fhgv.helpdeskapp.domainws.Acompanhamento;
import br.com.fhgv.helpdeskapp.domainws.Atendimento;
import br.com.fhgv.helpdeskapp.domainws.AtendimentoAnexo;
import br.com.fhgv.helpdeskapp.domainws.AtendimentoCombos;
import br.com.fhgv.helpdeskapp.domainws.IssueFilter;
import br.com.fhgv.helpdeskapp.domainws.PageResponse;
import br.com.fhgv.helpdeskapp.domainws.RecordResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IssuesWS {

    @POST("api/sla/painel-chamados/read/")
    Call<PageResponse<Atendimento>> getAll(@Body IssueFilter filter);

    @GET("api/sla/painel-chamados/read/{id}")
    Call<RecordResponse<Atendimento>> getById(@Path("id") int id);

    @PUT("api/sla/painel-chamados/{id}/upload-anexo")
    Call<RecordResponse<AtendimentoAnexo>> uploadAnexo(@Path("id") int id, @Body AtendimentoAnexo anexo);

    @PUT("api/sla/painel-chamados/update/iniciar/{id}")
    Call<RecordResponse<Atendimento>> iniciarAtendimento(@Path("id") int id);

    @PUT("api/sla/painel-chamados/update/concluir/{id}")
    Call<RecordResponse<Atendimento>> concluirAtendimento(@Path("id") int id, @Body Atendimento atendimento);

    @PUT("api/sla/painel-chamados/update/assumir/{id}")
    Call<RecordResponse<Atendimento>> roubarChamadoDoColeguinha(@Path("id") int id);

    @PUT("api/sla/painel-chamados/update/cancelar/{id}")
    Call<RecordResponse<Atendimento>> cancelarAtendimento(@Path("id") int id, @Body Atendimento atendimento);

    @PUT("api/sla/painel-chamados/update/pendente/{id}")
    Call<RecordResponse<Atendimento>> definirChamadoComoPendente(@Path("id") int id, @Body Atendimento atendimento);

    @PUT("api/sla/painel-chamados/update/resumir-atendimento/{id}")
    Call<RecordResponse<Atendimento>> pendenteParaEmAtendimento(@Path("id") int id, @Body Atendimento atendimento);

    @PUT("api/sla/painel-chamados/update/acompanhamento/save")
    Call<RecordResponse<Acompanhamento>> saveFollowUp(@Body Acompanhamento acompanhamento);

    @PUT("api/sla/painel-chamados/update/acompanhamento/delete")
    Call<RecordResponse<Acompanhamento>> deleteFollowUp(@Body Acompanhamento acompanhamento);
}
