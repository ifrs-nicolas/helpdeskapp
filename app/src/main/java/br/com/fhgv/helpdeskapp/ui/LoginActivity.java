package br.com.fhgv.helpdeskapp.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.helpdeskapp.domainws.User;
import br.com.fhgv.helpdeskapp.webservice.SLA;
import br.com.fhgv.util.AppUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private TextView message;
    private static User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        this.message = findViewById(R.id.mensagem);
        ((EditText) findViewById(R.id.senha)).setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                findViewById(R.id.buttonLogin).performClick();
                return true;
            }
            return false;
        });
    }

    public void login(View view) {
        EditText etLogin = findViewById(R.id.login);
        EditText etPassword = findViewById(R.id.senha);

        String username = etLogin.getText().toString();
        String password = etPassword.getText().toString().isEmpty() ? "" : etPassword.getText().toString();

        SLA.login(username, password);
        SLA.getAutenticacaoWS().getUser().enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                user = response.body();
                if (user != null && user.getError() == null) {
                    message.setText(String.format(Locale.getDefault(), "ID: %d - Login: %s autenticado com sucesso.", user.getId(), user.getLogin()));
                    AppUtils.saveUserInSharedPreferences(user);
                    finish();
                } else {
                    message.setText(String.format("%s: %s", getString(R.string.error), user == null ?
                            getString(R.string.auth_user_pass_error) : user.getError()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                message.setText(String.format("%s: %s", getString(R.string.error), t.getMessage()));
                Log.e("ERROR ", t.toString());
            }
        });
    }
}
