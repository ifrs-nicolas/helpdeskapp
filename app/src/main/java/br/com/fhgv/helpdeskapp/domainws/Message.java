package br.com.fhgv.helpdeskapp.domainws;

import java.io.Serializable;

import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.util.AppUtils;

public class Message implements Serializable {

    public String type;
    public String message;

    public Message() {
    }

    public Message(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        if ("SUCCESS".equalsIgnoreCase(type)) {
            type = AppUtils.getMessage(R.string.success);
        } else if ("ERROR".equalsIgnoreCase(type)) {
            type = AppUtils.getMessage(R.string.error);
        }
        return String.format("%s: %s", type, message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
