package br.com.fhgv.helpdeskapp.domainws;

import java.util.ArrayList;
import java.util.List;

public class AtendimentoCombos {

    private List<TipoAtendimento> tiposAtendimentos;
    private List<Area> areas;
    private List<Unidade> unidades;
    private List<Setor> setores;

    public AtendimentoCombos() {
    }

    public List<TipoAtendimento> getTiposAtendimentos() {
        return tiposAtendimentos;
    }

    public void setTiposAtendimentos(List<TipoAtendimento> tiposAtendimentos) {
        this.tiposAtendimentos = tiposAtendimentos;
    }

    public List<Area> getAreas() {
        return areas;
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    public List<Unidade> getUnidades() {
        return unidades;
    }

    public void setUnidades(List<Unidade> unidades) {
        this.unidades = unidades;
    }

    public List<Setor> getSetores() {
        return setores;
    }

    public void setSetores(List<Setor> setores) {
        this.setores = setores;
    }
}
