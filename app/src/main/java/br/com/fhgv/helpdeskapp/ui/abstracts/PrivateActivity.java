package br.com.fhgv.helpdeskapp.ui.abstracts;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.helpdeskapp.domainws.User;
import br.com.fhgv.helpdeskapp.ui.IssuesPanelActivity;
import br.com.fhgv.helpdeskapp.ui.LoginActivity;
import br.com.fhgv.helpdeskapp.ui.MyIssuesActivity;
import br.com.fhgv.helpdeskapp.ui.NewIssueActivity;
import br.com.fhgv.util.AppUtils;

public abstract class PrivateActivity extends BaseActivity {

    protected User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_private, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                AppUtils.removeUser();
                startActivity(new Intent(getApplicationContext(), getClass()));
                this.finish();
                return true;
            case R.id.newIssue:
                startActivity(new Intent(getApplicationContext(), NewIssueActivity.class));
                if (getClass().equals(NewIssueActivity.class))
                    this.finish();
                return true;
            case R.id.issuePanel:
                startActivity(new Intent(getApplicationContext(), IssuesPanelActivity.class));
                if (getClass().equals(IssuesPanelActivity.class))
                    this.finish();
                return true;
            case R.id.myIssues:
                startActivity(new Intent(getApplicationContext(), MyIssuesActivity.class));
                if (getClass().equals(MyIssuesActivity.class))
                    this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected boolean verifyUser() {
        this.user = AppUtils.getUser();
        if (user == null) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            return false;
        } else {
            if (menu != null){
                MenuItem item = menu.findItem(R.id.issuePanel);
                item.setVisible(AppUtils.userIsTecnico());
            }
            return true;
        }
    }
}
