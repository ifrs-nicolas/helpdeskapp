package br.com.fhgv.helpdeskapp.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.fhgv.helpdeskapp.R;
import br.com.fhgv.helpdeskapp.domainws.Acompanhamento;
import br.com.fhgv.util.AppUtils;

public class AcompanhamentoRecyclerAdapter extends RecyclerView.Adapter<AcompanhamentoRecyclerAdapter.AcompanhamentoLineHolder> {

    private List<Acompanhamento> list;

    public AcompanhamentoRecyclerAdapter(List<Acompanhamento> list) {
        this.list = list;
    }

    public void addItem(Acompanhamento acompanhamento) {
        if (list == null) {
            list = new ArrayList<>();
        }
        int oldSize = getItemCount();
        this.list.add(acompanhamento);
        this.notifyItemRangeInserted(oldSize, getItemCount());
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    @NonNull
    public AcompanhamentoLineHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AcompanhamentoLineHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.follow_up_line_view, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AcompanhamentoLineHolder viewHolder, int position) {
        if (list != null) viewHolder.loadData(list.get(position));
    }

    public class AcompanhamentoLineHolder extends RecyclerView.ViewHolder {

        public TextView tvData, tvLogin, tvTexto;

        private AcompanhamentoLineHolder(View itemView) {
            super(itemView);
            tvData = itemView.findViewById(R.id.tv1);
            tvLogin = itemView.findViewById(R.id.tvInfo);
            tvTexto = itemView.findViewById(R.id.tvTexto);
        }

        private void loadData(Acompanhamento item) {
            this.tvData.setText(AppUtils.brDate(item.getDataCadastro()));
            this.tvLogin.setText(item.getTecnico() == null ? "" : item.getTecnico().getLogin());
            this.tvTexto.setText(item.getTexto());
        }
    }
}
