package br.com.fhgv.helpdeskapp.domainws;

import java.io.Serializable;
import java.util.List;

public class PageResponse<T> implements Serializable {

    private List<T> recordsList;
    private Message message;
    private long recordsNumber;
    private int totalPages;
    private long totalElements;

    public PageResponse() {
    }

    public PageResponse(List<T> recordsList, Message message, long recordsNumber, int totalPages, long totalElements) {
        this.recordsList = recordsList;
        this.message = message;
        this.recordsNumber = recordsNumber;
        this.totalPages = totalPages;
        this.totalElements = totalElements;
    }

    public List<T> getRecordsList() {
        return recordsList;
    }

    public void setRecordsList(List<T> recordsList) {
        this.recordsList = recordsList;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public long getRecordsNumber() {
        return recordsNumber;
    }

    public void setRecordsNumber(long recordsNumber) {
        this.recordsNumber = recordsNumber;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(long totalElements) {
        this.totalElements = totalElements;
    }
}
