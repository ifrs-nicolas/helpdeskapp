package br.com.fhgv.helpdeskapp.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

public class ServiceInitialize extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.v("SLA", "SERVICE - ServiceInitialize onReceive()");
        startService(context, NovoChamadoService.class);
    }

    private static void startService(Context context, Class<?> cls) {
        Intent service = new Intent(context, cls);
        context.stopService(service);
//      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//          ContextCompat.startForegroundService(context, service);
//      else
        context.startService(service);
    }
}
