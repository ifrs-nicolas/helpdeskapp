package br.com.fhgv.helpdeskapp;

public class CONST {

    public static final String APP_IMAGES_FOLDER = "FHGVHelpdeskFiles";
    public static final String FILE_PROVIDER_AUTHORITY = "br.com.fhgv.helpdeskapp";

//    public static final String SERVER_IP = "192.168.1.219:8080";            // servidor de testes interno
//    public static final String SERVER_IP = "sistemas.fhgv.com.br:9000";   // servidor de testes externo
//    public static final String SERVER_IP = "192.168.1.6:8080";            // servidor de produção interno
    public static final String SERVER_IP = "sistemas.fhgv.com.br:8080";   // servidor de produção externo

    public static final String WEBSERVICE_URL = "http://" + SERVER_IP + "/suporte/";
    public static final String SOCKET_URL = "ws://" + SERVER_IP + "/suporte/websocket/websocket";
    public static final String WEBSOCKET_PAINEL = "/socket-receive/sla/painel-chamados/atualizar/";
    public static final String WEBSOCKET_NOVO_CHAMADO_CADASTRADO = "/socket-call/sla/novo-chamado/atualizar-painel-chamados/";

    public static final String INIT_SERVICES_ACTION = "br.com.fhgv.helpdeskapp.service.initialize";

}
